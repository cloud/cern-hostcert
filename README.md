> :warning: This repository [has moved](https://gitlab.cern.ch/kubernetes/security/cern-hostcert). The new location is a fork and not a repository migration so that old container registry images were kept.

# What's in this image

This image sets up the krb host certificate for a CERN host.

## Atomic

The easiest way to use this is to launch it as an Atomic system container.
```
atomic install --system gitlab-registry.cern.ch/cloud/cern-hostcert
systemctl start cern-hostcert
```

The host certificate files will be placed on the host in the usual location - /etc/grid-security.

## Docker

If you prefer to launch it as a regular Docker container, here's the command:
```
docker run --name cern-hostcert --rm --net host \
	-v /etc:/etc-host \
	gitlab-registry.cern.ch/cloud/cern-hostcert
```

## Tags

We keep both 'qa' and 'latest' tags, used for rolling out upgrades.

# Issues

# Contributing

You are invited to contribute new features and fixes.

[Check here](https://gitlab.cern.ch/cloud/cern-hostcert) for the original Dockerfile and repository.

# Authors

Ricardo Rocha [ricardo.rocha@cern.ch](mailto:ricardo.rocha@cern.ch)

