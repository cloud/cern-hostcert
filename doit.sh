#!/bin/bash
set -x

mkdir -p /etc-host/grid-security

_hostname="$(hostname -s).cern.ch"
i=0
while [[ ! -f /etc-host/grid-security/hostcert.pem ]] || [[ ! -f /etc-host/grid-security/hostkey.pem ]] ;
do
    ((i++))
    echo "INFO Attempt to generate certificates: ${i}"
    cern-get-certificate --verbose --autoenroll --force --grid --hostname "${_hostname}"

    cp "/etc-host/grid-security/${_hostname}.grid.pem" /etc-host/grid-security/hostcert.pem
    cp "/etc-host/grid-security/${_hostname}.grid.key" /etc-host/grid-security/hostkey.pem
    sleep 10s
done

if [ ${CERNHOSTCERT_NOSLEEP} != "" ]; then
	exit 0
fi

while true; do sleep 1000; done
