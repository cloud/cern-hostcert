FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest

MAINTAINER Ricardo Rocha <ricardo.rocha@cern.ch>

LABEL name="cern-hostcert" \
      maintainer="Ricardo Rocha <ricardo.rocha@cern.ch>" \
      license="UNKNOWN" \
      summary="CERN host certificate setup container" \
      version="0.1" \
      help="None" \
      architecture="x86_64" \
      atomic.type="system" \
      distribution-scope="public"

RUN yum install -y \
	bind-utils \
	cern-get-certificate \
	curl \
	krb5-workstation \
	hostname \
	&& yum clean all

RUN echo $'\n\
keypath=/etc-host/grid-security \n\
certpath=/etc-host/grid-security \n\
uid=0 \n\
gid=0 \n\
autorenew=0 \n\
fetchcrl=1 \n\
days=7 \n\
keytab=/etc-host/krb5.keytab \n'\
>> /cern-get-certificate-conf

# FIXME: this is a workaround for https://its.cern.ch/jira/browse/LOS-154
RUN sed -i "s#CERT_CONFIG =>.*#CERT_CONFIG => '/cern-get-certificate-conf',#" /usr/sbin/cern-get-certificate

COPY service.template config.json.template manifest.json tmpfiles.template /exports/

ADD doit.sh /doit.sh

CMD ["/doit.sh"]
